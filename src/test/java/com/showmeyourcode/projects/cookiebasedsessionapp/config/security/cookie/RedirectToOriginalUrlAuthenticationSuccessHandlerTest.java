package com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie;

import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.WebSecurityConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RedirectToOriginalUrlAuthenticationSuccessHandlerTest {

  @Mock
  private HttpServletRequest request;
  @Mock
  private HttpServletResponse response;
  @Mock
  private Authentication authentication;
  @Mock
  private UserInfo userInfo;

  @InjectMocks
  private RedirectToOriginalUrlAuthenticationSuccessHandler handler;

  @Test
  void determineTargetUrl_returnsTargetUrlFromRequest() {
    when(request.getParameter(WebSecurityConfig.TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM)).thenReturn("/target");

    var targetUrl = handler.determineTargetUrl(request, response, authentication);

    assertThat(targetUrl).isEqualTo("/target");
  }

  @Test
  void determineTargetUrl_suppressAbsolutUrls() {
    when(request.getParameter(WebSecurityConfig.TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM)).thenReturn("http://www.google.de");

    var targetUrl = handler.determineTargetUrl(request, response, authentication);

    assertThat(targetUrl).isEqualTo("/");
  }

  @Test
  void onAuthenticationSuccess_addsColourToUserInfo() throws IOException, ServletException {
    when(authentication.getPrincipal()).thenReturn(userInfo);
    when(request.getParameter("colour")).thenReturn("YELLOW");

    handler.onAuthenticationSuccess(request, response, authentication);

    verify(userInfo).setColour("YELLOW");
  }

}
