package com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginWithTargetUrlAuthenticationEntryPointTest {

  @Mock
  private HttpServletRequest request;
  @Mock
  private HttpServletResponse response;

  private final LoginWithTargetUrlAuthenticationEntryPoint entryPoint = new LoginWithTargetUrlAuthenticationEntryPoint();

  @Test
  void appends_targetURL() {
    when(request.getRequestURI()).thenReturn("/original/url");

    String url = entryPoint.determineUrlToUseForThisRequest(request, response, null);

    assertThat(url).isEqualTo("/login?target=/original/url");
  }
}
