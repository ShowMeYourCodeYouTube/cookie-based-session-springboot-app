package com.showmeyourcode.projects.cookiebasedsessionapp;

import com.showmeyourcode.projects.cookiebasedsessionapp.config.WebMvcConfiguration;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.WebSecurityConfig;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.CookieSecurityContextRepository;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.InMemoryAuthenticationProvider;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.LoginWithTargetUrlAuthenticationEntryPoint;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.RedirectToOriginalUrlAuthenticationSuccessHandler;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.SignedUserInfoCookie;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.exception.CookieVerificationFailedException;
import jakarta.servlet.http.Cookie;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import({
  WebMvcConfiguration.class,
  CookieSecurityContextRepository.class,
  InMemoryAuthenticationProvider.class,
  LoginWithTargetUrlAuthenticationEntryPoint.class,
  RedirectToOriginalUrlAuthenticationSuccessHandler.class,
  WebSecurityConfig.class
})
@WebMvcTest
@ActiveProfiles("test")
class ViewControllerIT {

  private final Cookie VALID_COOKIE = new Cookie(SignedUserInfoCookie.NAME, "uid=bob&roles=USER|TESTER&colour=darkBlue&hmac=JSvI/RXxuYNvlxt+Qtd7FcVMRID258LukTtoVhsBPlbutUSy6Q4MK3UDHq5LoC791aWD1fUi8cITLp4JjZui4Q==");
  private final String DEFAULT_LOGIN_URL = "http://localhost/login?target=/";
  private final String DEFAULT_ERROR_URL = "/login?error";
  @Autowired
  protected MockMvc mockMvc;

  @Test
  void shouldAuthenticateWhenUserProvideCorrectCredentials() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.post(WebMvcConfiguration.LOGIN_PATH)
        .contentType(APPLICATION_FORM_URLENCODED)
        .param("username", "bob")
        .param("password", "builder")
      )
      .andDo(print())
      .andExpect(status().isFound())
      .andExpect(MockMvcResultMatchers.cookie().value(SignedUserInfoCookie.NAME, is(notNullValue())));
  }

  @Test
  void shouldDisplayLoginPageWithLogoutInfoWhenUrlContainsLogoutPath() throws Exception {
    String url = "http://localhost/login?logout";

    this.mockMvc
      .perform(get(url))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(content().string("""
        <!DOCTYPE html>
        <html lang="en"
              xmlns="http://www.w3.org/1999/xhtml"
        >
        <head>
            <meta charset="UTF-8">
            <title>Login</title>
        </head>
        <body>
            <h1>Login</h1>

           \s
            <div>
                You have been logged out.
            </div>
            <form action="/login" method="post">
                <label for="username">Username</label>
                <input id="username" name="username" type="text">
                <label for="password">Password</label>
                <input id="password" name="password" type="password">
                <label for="colour">Colour</label>
                <input id="colour" name="colour" type="text">
                <button type="submit">Login</button>
                <input type="hidden" name="target" value="">
            </form>

            <a href="/">Home</a>
            <a href="/other">Other</a>
        </body>
        </html>
        """));
  }

  @Test
  void shouldLogoutAndClearCookieWhenUserProvideCorrectCredentials() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.get("/logout").cookie(VALID_COOKIE))
      .andDo(print())
      .andExpect(status().isFound())
      .andExpect(MockMvcResultMatchers.cookie().value(SignedUserInfoCookie.NAME, is(nullValue())));
  }

  @Test
  void shouldNotAuthenticateWhenUserProvideInvalidPassword() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.post(WebMvcConfiguration.LOGIN_PATH)
        .contentType(APPLICATION_FORM_URLENCODED)
        .param("username", "bob")
        .param("password", "builder-wrong")
      )
      .andDo(print())
      .andExpect(status().isFound())
      .andExpect(redirectedUrl(DEFAULT_ERROR_URL))
      .andExpect(MockMvcResultMatchers.cookie().doesNotExist(SignedUserInfoCookie.NAME));
  }

  @Test
  void shouldNotAuthenticateWhenUserProvideInvalidUser() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.post(WebMvcConfiguration.LOGIN_PATH)
        .contentType(APPLICATION_FORM_URLENCODED)
        .param("username", "any-user")
        .param("password", "builder")
      )
      .andDo(print())
      .andExpect(status().isFound())
      .andExpect(redirectedUrl(DEFAULT_ERROR_URL))
      .andExpect(MockMvcResultMatchers.cookie().doesNotExist(SignedUserInfoCookie.NAME));
  }

  @Test
  void shouldRedirectToIndexWhenUserIsAuthenticated() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.get(WebMvcConfiguration.DEFAULT_PATH).cookie(VALID_COOKIE))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(MockMvcResultMatchers.cookie().exists(SignedUserInfoCookie.NAME))
      .andExpect(content().contentType("text/html;charset=UTF-8"))
      .andExpect(content().string("""
        <!DOCTYPE html>
        <html lang="en"
              xmlns="http://www.w3.org/1999/xhtml"
        >
        <head>
            <meta charset="UTF-8">
            <title>Home</title>
        </head>
        <body>
        <h1>Hello bob</h1>
            <div>Your authorities: [TESTER, USER]</div>
            <br>

            <form action="/logout" method="post">
                <button type="submit">Logout</button>
            </form>

            <a href="/other">Other</a>
        </body>
        </html>
         """));
  }

  @Test
  void shouldRedirectToLoginWhenUserIsNotAuthenticated() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.get(WebMvcConfiguration.DEFAULT_PATH))
      .andDo(print())
      .andExpect(status().isFound())
      .andExpect(redirectedUrl(DEFAULT_LOGIN_URL))
      .andExpect(header().string(HttpHeaders.LOCATION, DEFAULT_LOGIN_URL));
  }

  @Test
  void shouldRedirectToOtherWhenUserIsAuthenticated() throws Exception {
    this.mockMvc
      .perform(MockMvcRequestBuilders.get(WebMvcConfiguration.OTHER_PATH).cookie(VALID_COOKIE))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(MockMvcResultMatchers.cookie().exists(SignedUserInfoCookie.NAME))
      .andExpect(content().contentType("text/html;charset=UTF-8"))
      .andExpect(content().string("""
        <!DOCTYPE html>
        <html lang="en"
              xmlns="http://www.w3.org/1999/xhtml"
        >
        <head>
            <meta charset="UTF-8">
            <title>Other</title>
        </head>
        <body>
            <h1>Other</h1>
            <div>Your username: bob</div>
            <br>

            <form action="/logout" method="post">
                <button type="submit">Logout</button>
            </form>

            <a href="/">Home</a>
        </body>
        </html>
        """));
  }

  @Test
  void shouldThrownInvalidSignatureExceptionWhenUserCookieIsInvalid() {
    Cookie cookie = new Cookie("UserInfo", "uid=bob&roles=USER|TESTER&colour=white&hmac=HbzgSN7SDjBsKJXCWRKB4RLgFGfHXjZKkcO52yX4FkqQPd88qKf9DNBCi59yAy+3RlYg6xCTrPZfJznRgnFxow==");

    var exception = assertThrows(
      CookieVerificationFailedException.class,
      () -> this.mockMvc.perform(MockMvcRequestBuilders.get(WebMvcConfiguration.DEFAULT_PATH).cookie(cookie))
    );

    assertTrue(exception.getMessage().contains("Cookie signature (HMAC) invalid"));
  }
}
