package com.showmeyourcode.projects.cookiebasedsessionapp.config.security;

import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.CookieSecurityContextRepository;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.InMemoryAuthenticationProvider;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.LoginWithTargetUrlAuthenticationEntryPoint;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.RedirectToOriginalUrlAuthenticationSuccessHandler;
import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.SignedUserInfoCookie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig  {

  public static final String LOGIN_FORM_URL = "/login";
  public static final String TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM = "target";
  public static final String COLOUR_PARAM = "colour";

  private final CookieSecurityContextRepository cookieSecurityContextRepository;
  private final LoginWithTargetUrlAuthenticationEntryPoint loginWithTargetUrlAuthenticationEntryPoint;
  private final RedirectToOriginalUrlAuthenticationSuccessHandler redirectToOriginalUrlAuthenticationSuccessHandler;
  private final InMemoryAuthenticationProvider inMemoryAuthenticationProvider;

  protected WebSecurityConfig(CookieSecurityContextRepository cookieSecurityContextRepository,
                              LoginWithTargetUrlAuthenticationEntryPoint loginWithTargetUrlAuthenticationEntryPoint,
                              RedirectToOriginalUrlAuthenticationSuccessHandler redirectToOriginalUrlAuthenticationSuccessHandler,
                              InMemoryAuthenticationProvider inMemoryAuthenticationProvider) {
    super();
    this.cookieSecurityContextRepository = cookieSecurityContextRepository;
    this.loginWithTargetUrlAuthenticationEntryPoint = loginWithTargetUrlAuthenticationEntryPoint;
    this.redirectToOriginalUrlAuthenticationSuccessHandler = redirectToOriginalUrlAuthenticationSuccessHandler;
    this.inMemoryAuthenticationProvider = inMemoryAuthenticationProvider;
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http
      // deactivate session creation
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and().csrf().disable()

      // store SecurityContext in Cookie / delete Cookie on logout
      .securityContext().securityContextRepository(cookieSecurityContextRepository)
      .and().logout().permitAll().deleteCookies(SignedUserInfoCookie.NAME)

      // deactivate RequestCache and append originally requested URL as query parameter to login form request
      .and().requestCache().disable()
      .exceptionHandling().authenticationEntryPoint(loginWithTargetUrlAuthenticationEntryPoint)

      // configure form-based login
      .and().formLogin()
      .loginPage(LOGIN_FORM_URL)
      // after successful login forward user to originally requested URL
      .successHandler(redirectToOriginalUrlAuthenticationSuccessHandler)

      .and()
      .authorizeHttpRequests((authz) -> authz
        .requestMatchers(LOGIN_FORM_URL).permitAll()
        .requestMatchers("/**").authenticated()
      )
      .securityContext((securityContext) -> securityContext
        .requireExplicitSave(false)
      );

    return http.build();
  }

  @Bean
  public AuthenticationManager authManager(HttpSecurity http) throws Exception {
    AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(
      AuthenticationManagerBuilder.class
    );
    authenticationManagerBuilder.authenticationProvider(inMemoryAuthenticationProvider);
    return authenticationManagerBuilder.build();
  }

}
