package com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie.exception;

public class CookieVerificationFailedException extends RuntimeException {
    public CookieVerificationFailedException(String message) {
        super(message);
    }
}
