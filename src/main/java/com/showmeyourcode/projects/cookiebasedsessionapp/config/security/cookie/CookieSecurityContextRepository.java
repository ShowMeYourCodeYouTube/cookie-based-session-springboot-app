package com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Used by SecurityContextPersistenceFilter to obtain the context which should be used for the current thread of execution
 * and to store the context once it has been removed from thread-local storage and the request has completed.
 */
@Component
public class CookieSecurityContextRepository implements SecurityContextRepository {

  private static final Logger LOG = LoggerFactory.getLogger(CookieSecurityContextRepository.class);
  private static final String ANONYMOUS_USER = "anonymousUser";

  private final String cookieHmacKey;

  public CookieSecurityContextRepository(@Value("${auth.cookie.hmac-key}") String cookieHmacKey) {
    this.cookieHmacKey = cookieHmacKey;
  }

  @Override
  public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
    SecurityContext context = SecurityContextHolder.createEmptyContext();
    readUserInfoFromCookie(requestResponseHolder.getRequest()).ifPresent(userInfoCookie ->
      context.setAuthentication(
        new UsernamePasswordAuthenticationToken(userInfoCookie, userInfoCookie.getPassword(), userInfoCookie.getAuthorities())
      )
    );
    return context;
  }

  @Override
  public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
    Authentication authentication = context.getAuthentication();
    if (authentication == null) {
      LOG.debug("No securityContext.authentication, skip saveContext");
      return;
    }

    if (ANONYMOUS_USER.equals(authentication.getPrincipal())) {
      LOG.debug("Anonymous User SecurityContext, skip saveContext");
      return;
    }

    if (!(authentication.getPrincipal() instanceof UserInfo)) {
      LOG.warn("securityContext.authentication.principal of unexpected type {}, skip saveContext", authentication.getPrincipal().getClass().getCanonicalName());
      return;
    }

    UserInfo userInfo = (UserInfo) authentication.getPrincipal();
    SignedUserInfoCookie cookie = new SignedUserInfoCookie(userInfo, cookieHmacKey);
    cookie.setSecure(request.isSecure());
    response.addCookie(cookie);

    LOG.debug("SecurityContext for principal '{}' saved in Cookie", userInfo.getUsername());
  }

  @Override
  public boolean containsContext(HttpServletRequest request) {
    return readUserInfoFromCookie(request).isPresent();
  }

  private Optional<UserInfo> readUserInfoFromCookie(HttpServletRequest request) {
    return readCookieFromRequest(request)
      .map(this::createUserInfo);
  }

  private Optional<Cookie> readCookieFromRequest(HttpServletRequest request) {
    if (request.getCookies() == null) {
      LOG.debug("No cookies in request");
      return Optional.empty();
    }

    Optional<Cookie> maybeCookie = Stream.of(request.getCookies())
      .filter(c -> SignedUserInfoCookie.NAME.equals(c.getName()))
      .findFirst();

    if (maybeCookie.isEmpty()) {
      LOG.debug("No {} cookie in request", SignedUserInfoCookie.NAME);
    }

    return maybeCookie;
  }

  private UserInfo createUserInfo(Cookie cookie) {
    return new SignedUserInfoCookie(cookie, cookieHmacKey).getUserInfo();
  }
}
