package com.showmeyourcode.projects.cookiebasedsessionapp.config.security.cookie;

import com.showmeyourcode.projects.cookiebasedsessionapp.config.security.WebSecurityConfig;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Used by the ExceptionTranslationFilter to commence a form login authentication via the UsernamePasswordAuthenticationFilter.
 * Holds the location of the login form in the loginFormUrl property, and uses that to build a redirect URL to the login page.
 * Alternatively, an absolute URL can be set in this property and that will be used exclusively.
 */
@Component
public class LoginWithTargetUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

  public LoginWithTargetUrlAuthenticationEntryPoint() {
    super(WebSecurityConfig.LOGIN_FORM_URL);
  }

  @Override
  protected String determineUrlToUseForThisRequest(
    HttpServletRequest request,
    HttpServletResponse response,
    AuthenticationException exception
  ) {
    return UriComponentsBuilder.fromUriString(super.determineUrlToUseForThisRequest(request, response, exception))
      .queryParam(WebSecurityConfig.TARGET_AFTER_SUCCESSFUL_LOGIN_PARAM, request.getRequestURI())
      .toUriString();
  }
}
