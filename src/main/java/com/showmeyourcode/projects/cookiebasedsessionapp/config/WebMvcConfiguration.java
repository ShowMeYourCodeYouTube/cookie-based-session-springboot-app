package com.showmeyourcode.projects.cookiebasedsessionapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

  public static final String DEFAULT_PATH = "/";
  public static final String OTHER_PATH = "/other";
  public static final String LOGIN_PATH = "/login";


  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController(DEFAULT_PATH).setViewName("index");
        registry.addViewController(OTHER_PATH).setViewName("other");
        registry.addViewController(LOGIN_PATH).setViewName("login");
    }
}
