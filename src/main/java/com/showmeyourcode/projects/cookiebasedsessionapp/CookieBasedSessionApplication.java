package com.showmeyourcode.projects.cookiebasedsessionapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookieBasedSessionApplication {

  public static void main(String[] args) {
    SpringApplication.run(CookieBasedSessionApplication.class, args);
  }

}
