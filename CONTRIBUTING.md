# Contributing rules

## Git commits

- Follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Tests

Remember about:

- using `IT` suffix for integration tests
- using`Test` suffix for unit tests

### Tests - MockMvc vs RestTemplate

Both MockMvc and RestTemplate are used for integration tests with Spring and JUnit.

Spring MVC Test builds on the mock request and response from spring-test and does not require a running servlet container. The main difference is that actual Spring MVC configuration is loaded through the TestContext framework and that the request is performed by actually invoking the DispatcherServlet and all the same Spring MVC infrastructure that is used at runtime.

Ref: https://itecnote.com/tecnote/java-difference-between-mockmvc-and-resttemplate-in-integration-tests/

## Maven commands

- Update the Maven Wrapper
    - `mvn wrapper:wrapper -Dmaven=3.9.6`
