# Cookie-based Session Spring-Boot App


| Branch |                                                                                                                   Pipeline                                                                                                                   |                                                                                                                Code coverage                                                                                                                 |                                           Jacoco test report                                            |                                 SonarCloud                                 |
|:------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/mirrors/cookie-based-session-springboot-app/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/mirrors/cookie-based-session-springboot-app/-/commits/master) | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/mirrors/cookie-based-session-springboot-app/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/mirrors/cookie-based-session-springboot-app/-/commits/master) | [link](https://showmeyourcodeyoutube.gitlab.io/mirrors/cookie-based-session-springboot-app/test-report) | [link](https://sonarcloud.io/organizations/showmeyourcodeyoutube/projects) |


---
A very simple Spring Boot app using Spring Security that 
stores user session information (e.g. username, roles) in a cookie instead of having a server-side persisted session.

*All credits to [INNOQ](https://github.com/innoq/cookie-based-session-springboot-app) / [blog post](https://innoq.com/en/blog/cookie-based-spring-security-session/)*. 
Current version of this project is maintained by ShowMeYourCode!

## Usage

Just as any other spring-boot app it can be started as follows

    mvn spring-boot:run
    
It listens on port 8080 and provides the following pages

* `/` - home page, requires authentication
* `/other` - other page, requires authentication
* `/login` - login form

It uses an in-memory authentication manager which knows exactly one set of valid credentials: 
`bob` / `builder`

---

1. Open `http://localhost:8080/other`
    * forward to `http://localhost:8080/login?target=/other` (login form)
    * a hidden input `target` contains originally requested URL
2. Log in with credentials
    * forward to `http://localhost:8080/other` (other page)
    * `UserInfo` cookie was set, value: `uid=bob&roles=TESTER|USER&hmac=...`
3. Open `http://localhost:8080/`
    * the home page is displayed (authentication still valid)
4. Log out
    * forward to a login form
    * a hidden input `target` is empty (no URL requested)
    * `UserInfo` cookie was deleted

## Technical solution (brief summary)

Details can be found in the code. The `WebSecurityConfig` class is a good entry point. 

### SessionCreationPolicy.STATELESS

See [spring-security | SessionCreationPolicy.html#STATELESS](https://docs.spring.io/spring-security/site/docs/5.3.3.RELEASE/api/org/springframework/security/config/http/SessionCreationPolicy.html#STATELESS).

Prevents the creation of the server-side session. CSRF is strongly coupled with the 
server-side session, so it has to be disabled as well to really activate the policy
(see https://github.com/spring-projects/spring-security/issues/5299).  

```java
  protected void configure(HttpSecurity http) throws Exception {
    http
      ...

      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and().csrf().disable()

      ...
  }
```

## Standard session-based authentication

Session-based authentication is a stateful authentication technique where we use sessions to keep track of the authenticated user.

Here is how Session Based Authentication works:
- User submits the login request for authentication.
- Server validates the credentials. If the credentials are valid, the server initiates a session and stores some information about the client. This information can be stored in memory, file system, or database. The server also generates a unique identifier that it can later use to retrieve this session information from the storage. Server sends this unique session identifier to the client.
- Client saves the session id in a cookie and this cookie is sent to the server in each request made after the authentication.
- Server, upon receiving a request, checks if the session id is present in the request and uses this session id to get information about the client.

Ref: https://roadmap.sh/guides/session-based-authentication

## HmacSHA512

In cryptography, an HMAC (sometimes expanded as either keyed-hash message authentication code or hash-based message authentication code) 
is a specific type of message authentication code (MAC) involving a cryptographic hash function and a secret cryptographic key. 
As with any MAC, it may be used to simultaneously verify both the data integrity and authenticity of a message.

SHA512 is a cryptographic hash. "SHA" is an acronym for "Secure Hash Algorithm". 
SHA512 is the strongest cryptographic hash in the SHA2 family.

Use HMAC-SHA512 for optimum speed, security and OK compatibility. 
HMAC-SHA256 is also very secure and could be useful on CPU's with 32 bit operations. 
Moreover, it is accelerated on many of the latest CPU's.

You can't decrypt hashes, and that's what HMACSHA512 is: a hasher. 
The point of a hash algorithm is that it is one-way.

Ref: https://en.wikipedia.org/wiki/HMAC
